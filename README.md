Finite Volume Method notes
==========================

An attempt to understand the reproduce a solvers using the Finite Volume Method
from first principles. I had to re-learn mathematics I haven't used for years,
and learn some I don't think I ever knew, so the gradient of the curve should
be fairly shallow and I've erred on the side of explaining almost everthing.

1. Make a virtual environment (I used Python 3.9)

2. Install dependencies

       $ pip install -r requirements.txt
   
3. Open Jupyter Lab

       $ jupyter lab
   
4. Look at the notebook `fvm1.ipynb` which is Best done in Chrome. It takes a
   while to render. The code is deliberately unoptimised (in fact de-optimised)
   for didactic reasons.
